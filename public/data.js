$(function () {
    console.log(Info);
    $("#cvo-profile-fullname").text(Info.fullname);
    $("#cvo-profile-title").text(Info.title);
    $("#cvo-profile-avatar").attr("src", Info.avatar);
    $("#cvo-profile-dob").text(Info.dob);
    $("#cvo-profile-gender").text(Info.gender);
    $("#cvo-profile-phone").text(Info.phone);
    $("#cvo-profile-email").text(Info.email);
    $("#cvo-profile-address").text(Info.address);
    $("#cvo-profile-website").text(Info.website);

    var html = "";
    Interest.forEach(element => {
        html += `<li class="row"><span class="cvo-interest-details">${element}</span></li>`;
    });
    $("#interest-table").html(html);
    $("#cvo-objective-objective").html(Objective)

    html = "";
    Skillrate.forEach(element => {
        html += `<div class="row"><div><span class="cvo-skillrate-title">${element.title}</span></div><div class="cvo-skillrate-bar" bval="${element.value}" rate-value="${element.value}"><span class="cvo-skillrate-value"></span></div></div>`;
    });
    $("#skillrate-table").html(html);

    html = "";
    Experience.forEach(element => {
        html += `<div class="row">
                    <hr>
                    <div class="cvo-experience-time">
                        <span class="time-background">
                            <span class="cvo-experience-start">${element.start}</span>
                        </span>
                    </div>
                    <div class="cvo-experience-company-wraper">
                        <span class="cvo-experience-company">${element.company}</span>
                    </div>`;
        element.project.forEach(sub => {
            html += `<div class="cvo-col-6">
                        <div class="cvo-block">
                            <div><span class="cvo-experience-position">${sub.name}</span></div>
                            <div class="cvo-experience-details" style="padding-left:10px">`;
            sub.detail.forEach(del => {
                html += `<span>${del}</span><br />`;
            });
            html += `<span>- Main responsibilities:</span><div class="cvo-block" style="padding: 0 0 0 10; ">`;
            sub.responsibilities.forEach(del => {
                html += `<span>${del}</span><br />`;
            });
            html += `</div>
                            </div>
                        </div>
                    </div>`;
        });

        html += `</div><div style="clear:both"></div>`;
    });
    $("#experience-table").html(html);
})

var Info = {
    fullname: "VO THANH NHUT",
    title: "Software Developer",
    avatar: "https://gitlab.com/nhutvt-public/nhutvt-public.gitlab.io/-/raw/master/z4075815144243_9388514a757af7c474414304cc03aae2.jpg",
    dob: "1994",
    gender: "Male",
    phone: "033 663 4045",
    email: "vtnhut94@gmail.com",
    address: "256/14/18 Phan Huy Ich Street, Ward 12, District Go Vap, HCM City",
    website: "vo.nhut3"
};

var Interest = ["Game(bi-a, cờ tướng)", "Music"];

var Objective = "I will use my accumulated experience to create the highest value for the company. At the same time, increase your personal experience to develop and serve the company. I hope to work in the dynamic and professional environment and have promotion opportunities.";

var Skillrate = [
    { title: "C# .NET(ext: ASP.NET MVC5, .NET Core, API,...)", value: 8 },
    { title: "HTML", value: 8 },
    { title: "Javascript(ext: Jquery, Reactjs, Angularjs, Nodejs)", value: 8 },
    { title: "Database(Oracle, Postgresql, SQL server, MongoDB)", value: 6 },
    { title: "Source Control(GIT, SVN)", value: 7 },
    { title: "CI/CD", value: 7 },
    { title: "Server(IIS, Nginx)", value: 7 },
    { title: "CSS(ext: Bootstrap)", value: 6 },
    { title: "Docker", value: 6 },
    { title: "Java", value: 7 },
];

var Experience = [];

Experience.push({
    start: "03/2018 - now",
    company: "Mobile World Investment Corporation",
    project: [
        {
            name: "The PIM Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 10",
                "- Technologies used: Java, Postgresql, Reactjs,...",
                "- Source control: Git, CI/CD, SVN,...",
                "- Server: Nginx",
                "- IDE: Visual Studio Code, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from BA.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Setup server.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The ERP Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 10",
                "- Technologies used: MVC5, .Net Core, C#, Oracle, Postgresql, Reactjs,...",
                "- Source control: Git, CI/CD, SVN,...",
                "- Server: IIS, Nginx",
                "- IDE: Visual studio, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from user.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Setup server.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The Mobile APP Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 10",
                "- Technologies used: MVC5, .Net Core, C#, Oracle, Postgresql, Reactjs,...",
                "- Source control: Git, CI/CD, SVN,...",
                "- Server: IIS, Nginx",
                "- IDE: Visual studio, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from user.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Setup server.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The Synchronized Data Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 3",
                "- Technologies used: MVC5, .Net Core, C#, Oracle, Postgresql, Reactjs,...",
                "- Source control: Git, CI/CD, SVN,...",
                "- Server: IIS, Nginx",
                "- IDE: Visual studio, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from user.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Setup server.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The EInvoice Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 3",
                "- Technologies used: MVC5, .Net Core, C#, Oracle, Postgresql, Reactjs,...",
                "- Source control: Git, CI/CD, SVN,...",
                "- Server: IIS, Nginx",
                "- IDE: Visual studio, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from user.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Setup server.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        }, {
            name: "The Inventory Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 6",
                "- Technologies used: Java, .Net Core, Oracle, React native,...",
                "- Source control: Git, CI/CD,...",
                "- Server: IIS, Nginx",
                "- IDE: Visual studio, Source tree,...",
            ],
            responsibilities: [
                "+ Join meeting to receive the request from user.",
                "+ Split task with team.",
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        }
    ]
})

Experience.push({
    start: "03/2017 - 03-2018",
    company: "NHAT CUONG SOFTWARE",
    project: [
        {
            name: "The ERP Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 8",
                "- Technologies used: MVC5,  C#, Oracle, MSSQL...",
                "- Source control: SVN,...",
                "- Server: IIS",
                "- IDE: Visual studio,...",
            ],
            responsibilities: [
                "+ Implementing and create test funtion.",
                "+ Support review code for team.",
                "+ Refactor code.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The CMS Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 8",
                "- Technologies used: MVC5, C#, MSSQL,...",
                "- Source control: SVN,...",
                "- Server: IIS",
                "- IDE: Visual studio,...",
            ],
            responsibilities: [
                "+ Implementing and create test funtion.",
                "+ Refactor code.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
    ]
})

Experience.push({
    start: "06/2016 - 03/2017",
    company: "SAIGON SOFTWARE",
    project: [
        {
            name: "The Hotel Manager Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 5",
                "- Technologies used: MVC5,  C#, MSSQL...",
                "- Source control: SVN",
                "- Server: IIS",
                "- IDE: Visual studio,...",
            ],
            responsibilities: [
                "+ Implementing and create test funtion.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
        {
            name: "The CMS Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 5",
                "- Technologies used: MVC5, C#, MSSQL,...",
                "- Source control: SVN",
                "- Server: IIS",
                "- IDE: Visual studio,...",
            ],
            responsibilities: [
                "+ Implementing and create test funtion.",
                "+ Checking error and fixbug.",
                "+ Support team members implementing and test."
            ]
        },
    ]
})

Experience.push({
    start: "01/2016 - 06/2016",
    company: "FREELANCER",
    project: [
        {
            name: "The CMS Project",
            detail: [
                "- Role: Developer",
                "- Team Size: 5",
                "- Technologies used: MVC5, C#, MSSQL,...",
                "- Source control: SVN",
                "- Server: IIS",
                "- IDE: Visual studio,...",
            ],
            responsibilities: [
                "+ Implementing and create test funtion.",
                "+ Checking error and fixbug.",
            ]
        },
    ]
})
